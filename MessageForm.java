/**
 * Created by Rail on 11.04.2016.
 */
public class MessageForm {
    private  String value;

    public MessageForm(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
