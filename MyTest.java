import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;


@Test
public class MyTest extends AuthBase {

    @DataProvider
    public Object[][] messageFormsFromXml() {
        ArrayList<MessageForm> result = new ArrayList<>();
        try {
            File fxmlFile = new File("message.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(fxmlFile);
            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("messages");
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                Element element = (Element) node;
                result.add((MessageForm) new MessageForm(element.getElementsByTagName("value").item(0).getTextContent()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Object[][] obj1 = (Object[][]) result.toArray();
        return obj1;
    }

    @Test(dataProvider = "messageFormsFromXml")
    public void openTopicTest(String value) {
        app.navigationHelper.goToAskPage();
        System.out.println(value
        );
        MessageForm message1 = new MessageForm(value);

        app.messageHelper.askQuestion(message1);
    }


    @Test(dataProvider = "messageFormsFromXml")
    public void commentYourTopicTest(String value) {
        app.navigationHelper.goToForumPage();
        System.out.println(value);
        app.navigationHelper.goToLinkByXpath("//div[3]/div/div[2]/a/div/span[2]");

        MessageForm message2 = new MessageForm(value);
        app.messageHelper.sendComment(message2);
    }

    @Test
    public void adminNavigateTest() {
        app.navigationHelper.goToAboutPage();

        app.navigationHelper.goToUsersPage();

        app.navigationHelper.goToProfilePage();
    }

    @Test(dataProvider = "messageFormsFromXml")
    public void deleteCommentTest(String value) {
        app.navigationHelper.goToForumPage();
        app.navigationHelper.goToLinkByXpath("//div[2]/div/div[2]/a/div/span[2]");
        MessageForm message3 = new MessageForm(value);
        app.messageHelper.sendComment(message3);
        app.messageHelper.deleteContent("span.glyphicon.glyphicon-trash");
    }
}